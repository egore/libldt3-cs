/*
 * Copyright 2016  Christoph Brill <egore911@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */using NodaTime;
using libldt3.attributes;
using libldt3.model.regel;

namespace libldt3
{
	namespace model
	{
		namespace objekte
		{

			/**
			 * Dieses Objekt enthält schwangerschaftsspezifische Informationen.
			 */
			[Objekt(Value = "0050")]
			public class Schwangerschaft
			{

				[Feld(Value = "8511", Feldart = Feldart.kann)]
				[Regelsatz(Value = new[] { typeof(F005) }, Laenge = 3)]
				public string schwangerschaftsdauer;
				[Feld(Value = "8512", Feldart = Feldart.muss)]
				[Regelsatz(Value = new[] { typeof(F018) }, Laenge = 8)]
				public LocalDate? ersterTagLetzterZyklus;
				[Feld(Value = "3471", Feldart = Feldart.bedingt_kann)]
				[Regelsatz(Value = new[] { typeof(F002) }, Laenge = 8)]
				public LocalDate? entbindungstermin;

			}
		}
	}
}
